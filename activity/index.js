// #1
const getCube = (number) => {
	console.log(`The cube of ${number} is ${number ** 3}`)
}

// #2
const address = ['123', 'Makahiya']
const [number, streetName] = address
console.log(`I'm from ${number} ${streetName} street.`)

// #3
const animal = {
	type: 'dog',
	name: 'Sam',
}
const { type, name } = animal
console.log(`I have a ${type} named ${name}.`)

// #4
const numbers = [1, 2, 3, 4, 5]
numbers.forEach((number) => console.log(number))

// #5
class Dog {
	constructor(name, age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
	}
}

const myDog = new Dog('Sam', 6, 'Shih Tzu')
console.log(myDog)
